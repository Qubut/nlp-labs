from typing import Tuple

import numpy as np
import pytorch_lightning as pl
from torch.utils.data import DataLoader, Dataset
from torch.utils.data import Dataset, random_split
from nlp_labs import _types as T


class IMDBreviews(Dataset):
    def __init__(
        self, vectors: np.ndarray[np.float32], sentiments: np.ndarray[np.float32]
    ):
        self.vectors = vectors
        self.sentiments = sentiments

    def __len__(self):
        return len(self.sentiments)

    def __getitem__(self, idx) -> Tuple[T.features, T.label]:
        return self.vectors[idx], self.sentiments[idx]


class IMDBDataModule(pl.LightningDataModule):
    def __init__(
        self,
        dataset: IMDBreviews,
        batch_size=64,
        test_size=0.2,
        val_size=0.2,
        n_workers=4,
    ):
        super().__init__()
        self.batch_size = batch_size
        self.dataset = dataset
        self.test_size = int(len(dataset) * test_size)
        self.val_size = int((len(dataset) - self.test_size) * val_size)
        self.n_workers = n_workers

    def setup(self, stage=None):
        dataset, self.test_dataset = random_split(
            dataset=self.dataset,
            lengths=[len(self.dataset) - self.test_size, self.test_size],
        )
        self.train_dataset, self.val_dataset = random_split(
            dataset=dataset, lengths=[int(len(dataset) - self.val_size), self.val_size]
        )

    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.n_workers,
        )

    def val_dataloader(self) -> DataLoader:
        return DataLoader(
            self.val_dataset, batch_size=self.batch_size, num_workers=self.n_workers
        )

    def test_dataloader(self) -> DataLoader:
        return DataLoader(
            self.test_dataset, batch_size=self.batch_size, num_workers=self.n_workers
        )
