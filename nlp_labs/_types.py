from typing import TypedDict

import numpy as np

word = str
similarity_index = float
label = np.float32
features = np.ndarray[np.float32]


class LoggerConfig(TypedDict):
    logdir: str
    name: str


class Word2VecConfig(TypedDict):
    window: int
    vector_size: int
    min_count: int
    workers: int
    batch_size: int
    test_size: float
    logger: LoggerConfig
    checkpointdir: str
    epochs: int
