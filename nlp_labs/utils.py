import os
from itertools import permutations
from pathlib import Path
from typing import List, Tuple, Any, IO

from expression.collections import Block, Seq
from gensim.models import KeyedVectors
from loguru import logger
from expression import Result, Error, Ok
from expression.collections.asyncseq import AsyncSeq
from nlp_labs import _types as T
import numpy as np
from gensim.models.word2vec import Word2Vec
from omegaconf import OmegaConf
import asyncio
import aiofiles
from nltk.tokenize import WordPunctTokenizer
from concurrent.futures import ThreadPoolExecutor


def load_config(path: Path):
    return OmegaConf.load(path)


def create_word2vec_model(
    corpus: List[str] | List[List[str]],
    config: T.Word2VecConfig,
    n_workers: int = os.cpu_count(),
) -> Word2Vec:
    return Word2Vec(
        sentences=corpus,
        window=config.window,
        vector_size=config.vector_size,
        min_count=config.min_count,
        workers=n_workers,
    )


def most_similar(
    word: T.word, topn: int, w2v_model: Word2Vec
) -> Result[List[Tuple[T.word, T.similarity_index]], KeyError]:
    try:
        result = w2v_model.wv.most_similar(word, topn=topn)
        return Ok(result)
    except KeyError as e:
        return Error(error=e)


def find_analogies(
    model: KeyedVectors, words: List[T.word]
) -> List[Result[Tuple, KeyError]]:
    def analogy(
        model: KeyedVectors, word1: T.word, word2: T.word, word3: T.word
    ) -> Result:
        try:
            result = model.most_similar(
                positive=[word3, word1], negative=[word2], topn=1
            )
            return Ok((word3, word1, result[0][0], word2))
        except KeyError as e:
            return Error(e)

    analogies = [
        analogy(model, word1, word2, word3)
        for word1, word2, word3 in permutations(words, 3)
    ]
    return analogies


def ensure_dir_exists(output_dir: Path):
    output_dir.mkdir(parents=True, exist_ok=True)


def tokenize_line(line: str, tokenizer: WordPunctTokenizer):
    tokenized_line = tokenizer.tokenize(line)
    formatted_line = " ".join(tokenized_line) + "\n"
    return formatted_line


def file_writer(data: Any, output_file: Path, mode: str = "a"):
    with open(output_file, mode) as file:
        file.write(data)


def process_file(input_path: Path, output_dir: Path, processor):
    output_path = output_dir / input_path.name
    ensure_dir_exists(output_dir)

    results = []
    with open(input_path, "r") as file:
        for line in file:
            results.append(processor(line, output_path=output_path))
    return results


def process_files(
    file_paths: List[Path], output_dir: Path, processor, n_workers: int = os.cpu_count()
):
    results = Block(file_paths).map(
        lambda path: process_file(path, output_dir, processor)
    )
    return results


def average_pooling(review, model):
    vectors = [model.wv[word] for word in review if word in model.wv]
    if vectors:
        return np.mean(vectors, axis=0)
    else:
        return np.zeros(model.vector_size)


def max_pooling(review, model):
    vectors = [model.wv[word] for word in review if word in model.wv]
    if vectors:
        return np.max(vectors, axis=0)
    else:
        return np.zeros(model.vector_size)
