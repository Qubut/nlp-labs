import pytorch_lightning as pl
import torch.nn.functional as F
import torch.nn as nn
import torch
from pprint import pp
from expression import compose
from sklearn.metrics import accuracy_score, f1_score
from torch.optim.lr_scheduler import StepLR


class SentimentAnalysisModel(pl.LightningModule):
    def __init__(self, input_dim=100, inference_steps=500, lr=0.001):
        super().__init__()
        self.linear = nn.Linear(input_dim, 1)
        self.sigmoid = nn.Sigmoid()
        self.f = compose(
            self.linear,
            self.sigmoid,
        )
        self.inference_steps = inference_steps
        self.lr = lr

    def forward(self, x: torch.Tensor):
        return self.f(x)

    def training_step(self, batch, batch_idx):
        data, targets = batch
        outputs = self(data.float())
        loss = F.binary_cross_entropy(outputs.squeeze(), targets.float())
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        data, targets = batch
        outputs = self(data.float())
        loss = F.binary_cross_entropy(outputs.squeeze(), targets.float())
        self.log("val_loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        return loss

    def test_step(self, batch, batch_idx):
        data, targets = batch
        outputs = self(data.float())
        loss = F.binary_cross_entropy(outputs.squeeze(), targets.float())
        self.log("test_loss", loss)
        predictions = torch.round(outputs.squeeze())
        accuracy = accuracy_score(
            targets.cpu().detach().numpy(), predictions.cpu().detach().numpy()
        )
        f1 = f1_score(
            targets.cpu().detach().numpy(), predictions.cpu().detach().numpy()
        )

        self.log("test_loss", loss)
        self.log("test_accuracy", accuracy, prog_bar=True)
        self.log("test_f1_score", f1, prog_bar=True)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(), lr=self.lr)
        scheduler = StepLR(optimizer, step_size=5, gamma=0.1)
        return {"optimizer": optimizer, "lr_scheduler": scheduler}

    # def on_epoch_end(self):
    #     train_loss = self.trainer.callback_metrics.get("train_loss").item()
    #     val_loss = self.trainer.callback_metrics.get("val_loss").item()

    #     pp("train_loss", train_loss)
    #     pp("validation_loss", val_loss)
